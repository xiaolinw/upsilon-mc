# file /afs/cern.ch/work/x/xiaolinw/GaussDev_v49r17/Gen/DecFiles/options/42112016.py generated: Sat, 08 May 2021 13:10:54
#
# Event Type: 42112016
#
# ASCII decay Descriptor: Upsilon(1S) -> mu+ mu-
#
from Gaudi.Configuration import *
importOptions( "$DECFILESROOT/options/YTransverse.py" )
from Configurables import Generation
Generation().EventType = 42112016
Generation().SampleGenerationTool = "Special"
from Configurables import Special
Generation().addTool( Special )
Generation().Special.ProductionTool = "Pythia8Production"
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/exclu_Upsilon1S,mm=coherent_starlight_evtGen_transverse.dec"
Generation().Special.CutTool = "DiLeptonInAcceptance"

from Configurables import Generation, Special, StarLightProduction
Generation().SampleGenerationTool = 'Special'
Generation().addTool(Special)
Generation().Special.CutTool = ''
Generation().Special.ProductionTool = 'StarLightProduction'
Generation().FullGenEventCutTool = 'DiLeptonInAcceptance'
Generation().Special.addTool(StarLightProduction)
Generation().Special.StarLightProduction.Commands += ['PROD_PID = 553013']
Generation().Special.StarLightProduction.Decays = False
Generation().Special.StarLightProduction.Commands += ['PROD_MODE = 2']

