# file /tmp/tmp.s3YxhDQ4ts/DBASE/Gen/DecFiles/v30r58/options/24142008.py generated: Wed, 21 Apr 2021 15:13:03
#
# Event Type: 24142008
#
# ASCII decay Descriptor: J/psi(1S) -> mu+ mu-
#
from Gaudi.Configuration import *
importOptions( "$DECFILESROOT/options/JpsiTransverse.py" )
from Configurables import Generation
Generation().EventType = 24142008
Generation().SampleGenerationTool = "SignalPlain"
from Configurables import SignalPlain
Generation().addTool( SignalPlain )
Generation().SignalPlain.ProductionTool = "Pythia8Production"
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/exclu_Jpsi,mm=coherent_starlight_evtGen_transverse.dec"
Generation().SignalPlain.CutTool = "DiLeptonInAcceptance"
Generation().SignalPlain.SignalPIDList = [ 443 ]

from Configurables import Generation, Special, StarLightProduction
Generation().SampleGenerationTool = 'Special'
Generation().addTool(Special)
Generation().Special.CutTool = ''
Generation().Special.ProductionTool = 'StarLightProduction'
Generation().FullGenEventCutTool = 'DiLeptonInAcceptance'
Generation().Special.addTool(StarLightProduction)
Generation().Special.StarLightProduction.Commands += ['PROD_PID = 443013']
Generation().Special.StarLightProduction.Decays = False
Generation().Special.StarLightProduction.Commands += ['PROD_MODE = 2']


# Ad-hoc particle gun code

from Configurables import ParticleGun
pgun = ParticleGun("ParticleGun")
pgun.SignalPdgCode = 443
pgun.DecayTool = "EvtGenDecay"
pgun.GenCutTool = "DiLeptonInAcceptance"

from Configurables import FlatNParticles
pgun.NumberOfParticlesTool = "FlatNParticles"
pgun.addTool( FlatNParticles , name = "FlatNParticles" )

from Configurables import MomentumSpectrum
pgun.ParticleGunTool = "MomentumSpectrum"
pgun.addTool( MomentumSpectrum , name = "MomentumSpectrum" )
pgun.MomentumSpectrum.PdgCodes = [ 443 ]
pgun.MomentumSpectrum.InputFile = "$PGUNSDATAROOT/data/Ebeam4000GeV/MomentumSpectrum_443.root"
pgun.MomentumSpectrum.BinningVariables = "ptpz"
pgun.MomentumSpectrum.HistogramPath = "flatPt2y"

from Configurables import BeamSpotSmearVertex
pgun.addTool(BeamSpotSmearVertex, name="BeamSpotSmearVertex")
pgun.VertexSmearingTool = "BeamSpotSmearVertex"
pgun.EventType = 24142008
