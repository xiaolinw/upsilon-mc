from Configurables import ToolSvc, EvtGenDecay
ToolSvc().addTool( EvtGenDecay )

ToolSvc().EvtGenDecay.PolarizedCharmonium = True
ToolSvc().EvtGenDecay.RealHelOne = 1.0
ToolSvc().EvtGenDecay.RealHelZero = 0.

from LHCbMath.Types import cpp
int_vec = cpp.vector("unsigned int")
mesons = int_vec()
mesons.push_back(100553)

ToolSvc().EvtGenDecay.CharmoniumBaseID = 100553
print ToolSvc().EvtGenDecay

